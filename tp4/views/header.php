<!doctype html>
<html>
    <head>
        <link rel="stylesheet"
               href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="../public/css/calendar.css">
        <title><?= isset($title) ? h($title) : 'Mon Calendrier'; ?></title>
    </head>

    <body>
        <nav class="navbar navbar-dark bg-primary mb-3">
            <a href="../public/index.php" class="navbar-brand">Page de connexion</a>
        </nav>
