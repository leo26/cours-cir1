<!DOCTYPE html>
<html>
    <head>
        <title>Page de log</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    </head>
    <body class="text-center">
        <?php
            require 'form.php';
            $form = new Form();
        ?>
        
        <form action="calendrier.php" method="POST" enctype="multipart/form-data">
            <div class="form-control">
                <h1>Connexion</h1>
                <?php
                    echo $form->input('Pseudo');
                    echo $form->input('Password');
                    echo $form->submit();
                ?>
            </div>
        </form>
        <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
    </body>
</html>
