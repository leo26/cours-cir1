<?php

public function e404 ()
{
  require '../public/404.php';
  exit();
}

public function get_pdo()
{
  return $pdo = new PDO('mysql:host=localhost;dbname=base', 'root', '', [
    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXEPTION,
    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
  ]);
}

public function h(?$value)
{
  if ($value === null) {
    return '';
  }
  return htmlentities($value);
}

function render($view, $parameters = [])
{
  extract($parameters);
  include "../views/{$views}.php";
}
