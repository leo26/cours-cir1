<?php

class form {

    function __construct($data = array()) {
        $this->data = $data;
    }
    
    public function input($name) {
        return /*'<label for="exampleInputEmail1">' . $name . ':</label>' . */
        '<p><input type="' . $name .'" name="' . $name . '" class="form-control" placeholder="' . $name . '"></p>';
    }
    
    public function submit() {
        return '<p><button type="submit" class="btn btn-lg btn-primary btn-block">Se connecter !</button></p>';
    }

}
