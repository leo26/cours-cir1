<?php

namespace Calendar;

class Event
{

  private $id;

  private $name;

  private $description;

  private $start;

  private $end;

  public function getId()
  {
    return $this->id;
  }

  public function getName ()
  {
    $this->name;
  }

  public function getDescription ()
  {
    $this->descritption ?? '';
  }

  public function getStart ()
  {
    return new \DateTime($this->start);
  }

  public function getEnd ()
  {
    return new \DateTime($this->end);
  }
}
