<head><title>Epicerie</title></head>
<form action="ajout_produits.php" enctype="multipart/form-data" method="post">
  <fieldset>
    <p>Photo de votre produit : <input type="file" name="photo"/></p>
    <?php
      $tmp_name = $_FILES["photo"]["tmp_name"];
      echo $tmp_name;
    ?>



    <p>Nom de votre produit : <input type="text" name="nom" /></p>
    <?php
    $nom = $_POST['nom'];
    echo htmlspecialchars($nom);
    ?>



    <p>Prix de votre produit : <input type="text" name="prix" /></p>
    <?php
    $prix = $_POST['prix'];
    if ($prix > 0) {
      echo number_format($prix, 2, ',', ' ');
    }
    else {
      echo 'ERROR';
    }
    ?>



    <p>Nombre de produit disponibles : <input type="text" name="nombre" /></p>
    <?php
    $nombre = (int)$_POST['nombre']
    echo $nombre;
    ?>



      <p><input type="submit" value="Envoyer"></p>
  </fieldset>
</form>

<?php
$lignes[] = array($tmp_name, $nom, $prix, $nombre);

$chemin = 'mesproduts.csv';
$delimiteur = ';';

$mesproduts_csv = fopen($chemin, 'a+');

foreach($lignes as $ligne){
	fputcsv($mesproduts_csv, $ligne, $delimiteur);
}

fclose($mesproduts_csv);
?>
