<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title> Chat </title>
    <link rel="stylesheet" href="style.css">
  </head>

  <body>
  <?php if(!empty($reponse)){ ?>
    <table>
      <?php
      foreach($reponse as $row){ ?>
        <tr>
          <td> <?php echo $row['Auteur']; ?> </td>
          <td class="messages"> <?php afficherMessage($row['Message'], $bdd); ?> </td>
          <td> <?php echo $row['Date']; ?> </td>
        </tr>
      <?php
      }
      $reponse->closeCursor();?>
    </table>
    <?php }
    ?>
    <form method="post" action="chat.php">
      <label for="auteur">Auteur :</label><input type="text" name="auteur" id="auteur">
      <label for="messages">Votre message :</label><textarea name="messages" id="messages"></textarea>
      <input type="submit" value="Envoyer">
    </form>
  </body>
