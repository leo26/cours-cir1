<?php
try {
 $bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'root', '');
} catch (Exception $e) {
    exit('Erreur');
}

function envoyerMessage($auteur, $messages, $bdd){
  $insertion = $bdd->prepare('INSERT INTO messages(Auteur, Message, Date) VALUES(:auteur, :messages, :date)');
  $insertion->execute(array(
    'auteur' => htmlspecialchars($auteur),
    'messages' => htmlspecialchars($messages),
    'date' => date('Y-m-d')
  ));
  $insertion->closeCursor();
}

function afficherMessage($messages, $bdd){
  $explodeString = explode(" ", $messages);
  if($explodeString[0] === '/me'){
    $explodeString[0] = '';
    $implodeString = implode(" ", $explodeString);
    echo '<em>' . $implodeString . '</em>';
  }

if(!empty($_POST['auteur']) && !empty($_POST['messages'])){
  envoyerMessage($_POST['auteur'], $_POST['messages'], $bdd);
}
$reponse = $bdd->query('SELECT * FROM messages');
include('chat_template.php');
